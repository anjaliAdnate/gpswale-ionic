webpackJsonp([18],{

/***/ 666:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TollMapPageModule", function() { return TollMapPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__toll_map__ = __webpack_require__(786);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var TollMapPageModule = /** @class */ (function () {
    function TollMapPageModule() {
    }
    TollMapPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__toll_map__["a" /* TollMapPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["IonicPageModule"].forChild(__WEBPACK_IMPORTED_MODULE_2__toll_map__["a" /* TollMapPage */]),
            ],
        })
    ], TollMapPageModule);
    return TollMapPageModule;
}());

//# sourceMappingURL=toll-map.module.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TollPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TollPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TollPage = /** @class */ (function () {
    function TollPage(navCtrl, navParams, apicalldaywise) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apicalldaywise = apicalldaywise;
        this.toggled = false;
        this.toggled = false;
        this.tollList();
    }
    TollPage.prototype.toggle = function () {
        this.toggled = !this.toggled;
    };
    TollPage.prototype.cancelSearch = function () {
        // this.toggle();
        this.toggled = false;
    };
    TollPage.prototype.callSearch = function (ev) {
        var _this = this;
        var searchKey = ev.target.value;
        var _baseURL;
        //_baseURL = this.apiCall.mainUrl + "devices/getDeviceByUser?email=" + this.islogin.email + "&id=" + this.islogin._id + "&skip=0&limit=10&search=" + searchKey + "&dealer=" + this.islogin._id;
        _baseURL = "https://www.oneqlik.in/toll/get?search=" + searchKey;
        this.apicalldaywise.callSearchService(_baseURL)
            .subscribe(function (data) {
            // this.apiCall.stopLoading();
            console.log("search result=> " + JSON.stringify(data));
            _this.list = data;
            //this.allDevices = data.devices;
            // console.log("fuel percentage: " + data.devices[0].fuel_percent)
        }, function (err) {
            console.log(err);
            // this.apiCall.stopLoading();
        });
    };
    TollPage.prototype.onClear = function (ev) {
        this.tollList();
        // this.getdevicesTemp();
        ev.target.value = '';
        // this.toggled = false;
    };
    TollPage.prototype.live = function () {
        this.navCtrl.push("TollMapPage");
    };
    TollPage.prototype.tollList = function () {
        var _this = this;
        console.log("inside", 123);
        var baseURLp = "https://www.oneqlik.in/toll/get";
        this.apicalldaywise.startLoading().present();
        this.apicalldaywise.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (data) {
            _this.apicalldaywise.stopLoading();
            console.log("toll list", data);
            _this.list = data;
        }, function (err) {
            _this.apicalldaywise.stopLoading();
            console.log(err);
        });
    };
    TollPage.prototype.addTolls = function () {
        this.navCtrl.push('TollAddTollPage');
    };
    TollPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TollPage');
    };
    TollPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-toll',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/gpswale-ionic_1/src/pages/toll/toll.html"*/'<!--\n\n  Generated template for the TollPage page.\n\n\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n\n  Ionic pages and navigation.\n\n-->\n\n<ion-header>\n\n\n\n  <ion-navbar>\n\n    <button ion-button menuToggle>\n\n      <ion-icon name="menu"></ion-icon>\n\n    </button>\n\n    <ion-title>Toll List</ion-title>\n\n    <!-- <ion-buttons end>\n\n      <button  ion-button icon-only>\n\n\n\n        <img src="assets/icon/stack-of-square-papers-svg.png">\n\n      </button>\n\n    </ion-buttons> -->\n\n\n\n    <ion-buttons end>\n\n      <button (click) = "live()" end>  Map View </button>\n\n      </ion-buttons>\n\n\n\n    <!-- <ion-buttons end>\n\n      <div>\n\n        <ion-icon name="radio-button-on"></ion-icon>\n\n        <ion-icon style="font-size: 2.2em;" color="light" *ngIf="!toggled" (click)="toggle()" name="search"></ion-icon>\n\n        <ion-searchbar *ngIf="toggled" (ionInput)="callSearch($event)" (ionClear)="onClear($event)"\n\n          (ionCancel)="cancelSearch($event)"></ion-searchbar>\n\n      </div>\n\n    </ion-buttons> -->\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content no-padding>\n\n<!-- <ion-buttons>\n\n<button (click) = "live()" end>  Map View </button>\n\n</ion-buttons> -->\n\n    <ion-card *ngFor="let item of list; let i = index">\n\n  <ion-item style="border-bottom: 2px solid #dedede;">\n\n\n\n    <ion-row>\n\n      <ion-col col-3>\n\n        <img src="assets/imgs/toll.png" style="\n\n          margin-top: -10px;\n\n          width: 40px;\n\n          height: 40px;" />\n\n      </ion-col>\n\n      <ion-col col-3>\n\n        <p style="\n\n          color:black;\n\n          font-size:16px;\n\n          margin: 0px;">\n\n          {{ item.tollName }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-6 style="text-align: end;">\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n         ">\n\n          {{item.tollTax}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n\n\n        ">\n\n          {{\'Toll Tax\'}}\n\n        </p>\n\n      </ion-col>\n\n      <!-- <ion-col col-4>\n\n        <p style="\n\n          text-align: center;\n\n          font-size: 11px;\n\n          color:#11c1f3;\n\n          font-weight:bold;">\n\n          {{item.Date}}\n\n        </p>\n\n      </ion-col> -->\n\n\n\n    </ion-row>\n\n\n\n    <!-- <ion-row style="margin-top:2%;">\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n         ">\n\n          {{item.tollTax}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        ">\n\n          {{\'Toll Tax\'}}\n\n        </p>\n\n      </ion-col>\n\n\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;">\n\n          {{item.location.coordinates[\'0\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:#53ab53;\n\n        font-weight:bold;">\n\n          {{\'Latitude\' }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n          margin-left: 8px;">\n\n          {{item.location.coordinates[\'1\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        margin-left: 8px;">\n\n          {{\'Longitude\'}}\n\n        </p>\n\n      </ion-col>\n\n    </ion-row> -->\n\n\n\n    <ion-row style="margin-top:2%;">\n\n\n\n      <ion-col col-4>\n\n        <p style="\n\n        color:gray;\n\n        font-size:11px;\n\n        font-weight:400;\n\n       ">\n\n          {{item.address}}\n\n        </p>\n\n\n\n        <p style="\n\n      color:#53ab53;\n\n      font-size: 11px;\n\n      font-weight: bold;\n\n      ">\n\n          {{\'Address\' }}\n\n        </p>\n\n      </ion-col>\n\n\n\n      <!-- <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;">\n\n          {{item.location.coordinates[\'0\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:#53ab53;\n\n        font-weight:bold;">\n\n          {{\'Latitude\' }}\n\n        </p>\n\n      </ion-col>\n\n      <ion-col col-4>\n\n        <p style="\n\n          color:gray;\n\n          font-size:11px;\n\n          font-weight:400;\n\n          margin-left: 8px;">\n\n          {{item.location.coordinates[\'1\']}}\n\n        </p>\n\n\n\n        <p style="\n\n        font-size: 11px;\n\n        color:red;\n\n        font-weight:bold;\n\n        margin-left: 8px;">\n\n          {{\'Longitude\'}}\n\n        </p>\n\n      </ion-col> -->\n\n\n\n    </ion-row>\n\n\n\n\n\n    <!--\n\n    -->\n\n\n\n    <!-- </ion-scroll> -->\n\n\n\n\n\n  </ion-item>\n\n </ion-card>\n\n  <!-- <ion-fab right bottom>\n\n    <button ion-fab color="gpsc" (click)="addTolls()">\n\n      <ion-icon name="add"></ion-icon>\n\n    </button>\n\n  </ion-fab> -->\n\n</ion-content>\n\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/gpswale-ionic_1/src/pages/toll/toll.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */]])
    ], TollPage);
    return TollPage;
}());

//# sourceMappingURL=toll.js.map

/***/ }),

/***/ 786:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TollMapPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_bottom_drawer__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Subscription__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_Subscription___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_Subscription__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__toll_toll__ = __webpack_require__(690);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var TollMapPage = /** @class */ (function () {
    function TollMapPage(navCtrl, navParams, apiCall, actionSheetCtrl, elementRef, 
    // private socialSharing: SocialSharing,
    alertCtrl, modalCtrl, plt, viewCtrl, translate) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.apiCall = apiCall;
        this.actionSheetCtrl = actionSheetCtrl;
        this.elementRef = elementRef;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.plt = plt;
        this.viewCtrl = viewCtrl;
        this.translate = translate;
        this.navigateButtonColor = '#006EC5'; //Default Color
        this.navColor = '#fff';
        this.policeButtonColor = '#006EC5';
        this.policeColor = '#fff';
        this.petrolButtonColor = "#006EC5";
        this.petrolColor = '#fff';
        this.shouldBounce = true;
        this.dockedHeight = 80;
        this.distanceTop = 200;
        this.drawerState = __WEBPACK_IMPORTED_MODULE_5_ion_bottom_drawer__["a" /* DrawerState */].Docked;
        this.states = __WEBPACK_IMPORTED_MODULE_5_ion_bottom_drawer__["a" /* DrawerState */];
        this.minimumHeight = 50;
        this.transition = '0.85s ease-in-out';
        this.ongoingGoToPoint = {};
        this.ongoingMoveMarker = {};
        this.data = {};
        this.socketSwitch = {};
        this.socketChnl = [];
        this.socketData = {};
        this.allData = {};
        this.titleText = 'multiple';
        this.isEnabled = false;
        this.showMenuBtn = false;
        this.mapHideTraffic = false;
        this.mapData = [];
        this.geodata = [];
        this.geoShape = [];
        this.locations = [];
        this.shwBckBtn = false;
        this.deviceDeatils = {};
        this.showaddpoibtn = false;
        this.zoomLevel = 18;
        this.tempMarkArray = [];
        this.nearbyPolicesArray = [];
        this.nearbyPetrolArray = [];
        this.socketurl = "https://www.oneqlik.in";
        this.displayNames = true;
        this.car = 'M17.402,0H5.643C2.526,0,0,3.467,0,6.584v34.804c0,3.116,2.526,5.644,5.643,5.644h11.759c3.116,0,5.644-2.527,5.644-5.644 V6.584C23.044,3.467,20.518,0,17.402,0z M22.057,14.188v11.665l-2.729,0.351v-4.806L22.057,14.188z M20.625,10.773 c-1.016,3.9-2.219,8.51-2.219,8.51H4.638l-2.222-8.51C2.417,10.773,11.3,7.755,20.625,10.773z M3.748,21.713v4.492l-2.73-0.349 V14.502L3.748,21.713z M1.018,37.938V27.579l2.73,0.343v8.196L1.018,37.938z M2.575,40.882l2.218-3.336h13.771l2.219,3.336H2.575z M19.328,35.805v-7.872l2.729-0.355v10.048L19.328,35.805z';
        this.carIcon = {
            path: this.car,
            labelOrigin: [25, 50],
            strokeColor: 'black',
            strokeWeight: .10,
            fillOpacity: 1,
            fillColor: 'blue',
            anchor: [12.5, 12.5],
        };
        this.icons = {
            "car": this.carIcon,
            "bike": this.carIcon,
            "truck": this.carIcon,
            "bus": this.carIcon,
            "user": this.carIcon,
            "jcb": this.carIcon,
            "tractor": this.carIcon,
            "ambulance": this.carIcon,
            "auto": this.carIcon
        };
        this.resumeListener = new __WEBPACK_IMPORTED_MODULE_6_rxjs_Subscription__["Subscription"]();
        this.hideMe = false;
        if (localStorage.getItem('Total_Vech') !== null) {
            this.t_veicle_count = JSON.parse(localStorage.getItem('Total_Vech'));
        }
        //this.callBaseURL();
        var selectedMapKey;
        if (localStorage.getItem('MAP_KEY') != null) {
            selectedMapKey = localStorage.getItem('MAP_KEY');
            if (selectedMapKey == this.translate.instant('Hybrid')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
            else if (selectedMapKey == this.translate.instant('Normal')) {
                this.mapKey = 'MAP_TYPE_NORMAL';
            }
            else if (selectedMapKey == this.translate.instant('Terrain')) {
                this.mapKey = 'MAP_TYPE_TERRAIN';
            }
            else if (selectedMapKey == this.translate.instant('Satellite')) {
                this.mapKey = 'MAP_TYPE_HYBRID';
            }
        }
        else {
            this.mapKey = 'MAP_TYPE_NORMAL';
        }
        this.userdetails = JSON.parse(localStorage.getItem('details')) || {};
        console.log("user details=> " + JSON.stringify(this.userdetails));
        this.menuActive = false;
        if (localStorage.getItem('DisplayVehicleName') !== null) {
            if (localStorage.getItem('DisplayVehicleName') == 'OFF') {
                this.displayNames = false;
            }
        }
    }
    TollMapPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        if (this.plt.is('ios')) {
            this.shwBckBtn = true;
            this.viewCtrl.showBackButton(false);
        }
        this.plt.ready().then(function () {
            _this.resumeListener = _this.plt.resume.subscribe(function () {
                var today, Christmas;
                today = new Date();
                Christmas = new Date(JSON.parse(localStorage.getItem("backgroundModeTime")));
                var diffMs = (today - Christmas); // milliseconds between now & Christmas
                var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
                if (diffMins >= 5) {
                    localStorage.removeItem("backgroundModeTime");
                    _this.alertCtrl.create({
                        message: _this.translate.instant("Its been 5 mins or more since the app was in background mode. Do you want to reload the screen?"),
                        buttons: [
                            {
                                text: _this.translate.instant('YES PROCEED'),
                                handler: function () {
                                    _this.refreshMe();
                                }
                            },
                            {
                                text: _this.translate.instant('Back'),
                                handler: function () {
                                    _this.navCtrl.setRoot('DashboardPage');
                                }
                            }
                        ]
                    }).present();
                }
            });
        });
    };
    TollMapPage.prototype.ionViewWillLeave = function () {
        var _this = this;
        this.plt.ready().then(function () {
            _this.resumeListener.unsubscribe();
        });
    };
    TollMapPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.navBar.backButtonClick = function (ev) {
            console.log('this will work in Ionic 3 +');
            _this.hideMe = true;
            _this.navCtrl.pop({
                animate: true, animation: 'transition-ios', direction: 'back'
            });
        };
        if (localStorage.getItem("navigationFrom") === null) {
            this.initiateMap(); // at first we will load the map..
        }
        if (localStorage.getItem("CLICKED_ALL") != null) {
            this.showMenuBtn = true;
        }
        if (localStorage.getItem("SCREEN") != null) {
            if (localStorage.getItem("SCREEN") === 'live') {
                this.showMenuBtn = true;
            }
        }
        this.drawerState = __WEBPACK_IMPORTED_MODULE_5_ion_bottom_drawer__["a" /* DrawerState */].Bottom;
        this.onClickShow = false;
        this.showBtn = false;
        this.SelectVehicle = "Select Vehicle";
        this.selectedVehicle = undefined;
    };
    TollMapPage.prototype.ngOnInit = function () { };
    TollMapPage.prototype.ngOnDestroy = function () {
        localStorage.removeItem("CLICKED_ALL");
    };
    TollMapPage.prototype.initiateMap = function () {
        if (this.allData.map) {
            this.allData.map.remove();
            this.allData.map = this.newMap();
        }
        else {
            this.allData.map = this.newMap();
        }
        /* Check if traffic mode is On/Off */
        if (localStorage.getItem('TrafficMode') !== null) {
            if (localStorage.getItem('TrafficMode') === 'ON') {
                this.mapHideTraffic = true;
                this.allData.map.setTrafficEnabled(true);
            }
        }
        this.callFunction();
    };
    TollMapPage.prototype.callFunction = function () {
        this.to = new Date().toISOString();
        var d = new Date();
        var a = d.setHours(0, 0, 0, 0);
        this.from = new Date(a).toISOString();
        this.userDevices();
    };
    TollMapPage.prototype.userDevices = function () {
        //var baseURLp;
        var that = this;
        var baseURLp = "https://www.oneqlik.in/toll/get";
        that.apiCall.startLoading().present();
        that.apiCall.getdevicesForAllVehiclesApi(baseURLp)
            .subscribe(function (resp) {
            that.apiCall.stopLoading();
            that.portstemp = resp;
            // console.log("list of vehicles :", that.portstemp)
            that.mapData = [];
            that.mapData = resp.map(function (d) {
                if (d.location !== undefined) {
                    return { lat: d.location.coordinates['0'], lng: d.location.coordinates['1'] };
                }
            });
            var dummyData;
            dummyData = resp.map(function (d) {
                if (d.location === undefined)
                    return;
                else {
                    return {
                        "position": {
                            "lat": d.location.coordinates['0'],
                            "lng": d.location.coordinates['1']
                        },
                        "name": d.tollName,
                        "icon": {
                            "url": that.getIconUrl(d),
                            "size": {
                                "width": 20,
                                "height": 40
                            }
                        }
                    };
                }
            });
            dummyData = dummyData.filter(function (element) {
                return element !== undefined;
            });
            console.log("dummy data: ", dummyData);
            // console.log("dummy data: ", this.dummyData());
            var bounds = new __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["g" /* LatLngBounds */](that.mapData);
            that.allData.map.moveCamera({
                target: bounds,
                zoom: 10
            });
            debugger;
            if (that.isSuperAdmin || that.isDealer) {
                for (var t = 0; t < dummyData.length; t++) {
                    console.log("check position: ", dummyData[t].position);
                }
                that.addCluster(dummyData);
            }
            else {
                if (resp.length > 10) {
                    console.log("customers are greater than 10");
                    that.addCluster(dummyData);
                }
                else {
                    console.log("last cond");
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    TollMapPage.prototype.getIconUrl = function (data) {
        var that = this;
        var iconUrl;
        if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) > 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) > 0) {
            if (that.plt.is('ios')) {
                iconUrl = 'www/assets/imgs/vehicles/running' + data.iconType + '.png';
            }
            else if (that.plt.is('android')) {
                iconUrl = './assets/imgs/vehicles/running' + data.iconType + '.png';
            }
        }
        else {
            if ((data.status.toLowerCase() === 'running' && Number(data.last_speed) === 0) || data.status.toLowerCase() === 'idling' && Number(data.last_speed) === 0) {
                if (that.plt.is('ios')) {
                    iconUrl = 'www/assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
                else if (that.plt.is('android')) {
                    iconUrl = './assets/imgs/vehicles/idling' + data.iconType + '.png';
                }
            }
            else {
                var stricon;
                if (data.status.toLowerCase() === 'out of reach') {
                    stricon = "outofreach";
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + stricon + data.iconType + '.png';
                    }
                }
                else {
                    if (that.plt.is('ios')) {
                        iconUrl = 'www/assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                    else if (that.plt.is('android')) {
                        iconUrl = './assets/imgs/vehicles/' + data.status.toLowerCase() + data.iconType + '.png';
                    }
                }
            }
        }
        return iconUrl;
    };
    TollMapPage.prototype.addCluster = function (data) {
        var _this = this;
        var small, large;
        if (this.plt.is('android')) {
            small = "./assets/markercluster/small.png";
            large = "./assets/markercluster/large.png";
        }
        else if (this.plt.is('ios')) {
            small = "www/assets/markercluster/small.png";
            large = "www/assets/markercluster/large.png";
        }
        var options = {
            markers: data,
            icons: [
                {
                    min: 3,
                    max: 9,
                    url: small,
                    size: {
                        height: 29,
                        width: 29
                    },
                    label: {
                        color: "white"
                    }
                },
                {
                    min: 10,
                    url: large,
                    size: {
                        height: 37,
                        width: 37
                    },
                    label: {
                        color: "white"
                    }
                }
            ],
            boundsDraw: false,
            maxZoomLevel: 15
        };
        // let markerCluster: MarkerCluster = this.allData.map.addMarkerClusterSync(options);
        this.allData.map.addMarkerCluster(options)
            .then(function (markercluster) {
            markercluster.on(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["d" /* GoogleMapsEvent */].MARKER_CLICK).subscribe(function (params) {
                var marker = params[1];
                marker.setTitle(marker.get("name"));
                marker.setSnippet(marker.get("address"));
                marker.showInfoWindow();
            });
            _this.saveMarkerCluster = markercluster;
        });
    };
    TollMapPage.prototype.onClickMap = function (maptype) {
        var that = this;
        if (maptype == 'SATELLITE' || maptype == 'HYBRID') {
            // this.ngOnDestroy();
            that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].HYBRID);
            // this.someFunc();
        }
        else {
            if (maptype == 'TERRAIN') {
                // this.ngOnDestroy();
                that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].TERRAIN);
                // this.someFunc();
            }
            else {
                if (maptype == 'NORMAL') {
                    // this.ngOnDestroy();
                    that.allData.map.setMapTypeId(__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].NORMAL);
                    // this.someFunc();
                }
            }
        }
    };
    TollMapPage.prototype.newMap = function () {
        var mapOptions = {
            controls: {
                compass: false,
                zoom: false,
                myLocation: true,
                myLocationButton: false,
            },
            mapTypeControlOptions: {
                mapTypeIds: [__WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["e" /* GoogleMapsMapTypeId */].ROADMAP, 'map_style']
            },
            gestures: {
                rotate: false,
                tilt: false
            },
            mapType: this.mapKey
        };
        var map = __WEBPACK_IMPORTED_MODULE_3__ionic_native_google_maps__["b" /* GoogleMaps */].create('map_canvas', mapOptions);
        if (this.plt.is('android')) {
            map.setPadding(20, 20, 20, 20);
        }
        return map;
    };
    TollMapPage.prototype.goBack = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__toll_toll__["a" /* TollPage */]);
    };
    TollMapPage.prototype.onClickMainMenu = function () {
        this.menuActive = !this.menuActive;
    };
    TollMapPage.prototype.refreshMe = function () {
        this.ngOnDestroy();
        this.ionViewDidEnter();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Navbar"])
    ], TollMapPage.prototype, "navBar", void 0);
    TollMapPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-toll-map',template:/*ion-inline-start:"/Users/apple/Desktop/white-labels/gpswale-ionic_1/src/pages/toll-map/toll-map.html"*/'<ion-header>\n  <ion-navbar>\n    <button *ngIf="showMenuBtn" ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <!-- <ion-buttons start *ngIf="shwBckBtn">\n      <button ion-button small (click)="goBack()">\n        <ion-icon name="arrow-back"></ion-icon>Back\n      </button>\n    </ion-buttons> -->\n    <ion-title>Map View</ion-title>\n    <!-- <ion-buttons end *ngIf="showBtn">\n      <button ion-button (click)="ionViewDidEnter()">\n        {{ "All Vehicles" | translate }}\n      </button>\n    </ion-buttons> -->\n  </ion-navbar>\n  <!-- <ion-item>\n    <ion-label>{{ "Select Vehicle" | translate }}</ion-label>\n    <select-searchable item-content [(ngModel)]="selectedVehicle" [items]="portstemp" itemValueField="Device_Name"\n      itemTextField="Device_Name" [canSearch]="true" (onChange)="temp(selectedVehicle)">\n    </select-searchable>\n  </ion-item> -->\n</ion-header>\n\n<ion-content style="background-color: #f2f1f0" *ngIf=\'!hideMe\'>\n\n  <div id="map_canvas">\n\n    <ion-fab top left>\n      <button ion-fab color="light" mini>\n        <ion-icon color="gpsc" name="map"></ion-icon>\n      </button>\n      <ion-fab-list side="bottom">\n        <button ion-fab (click)="onClickMap(\'SATELLITE\')" color="gpsc">\n          S\n        </button>\n        <button ion-fab (click)="onClickMap(\'TERRAIN\')" color="gpsc">\n          T\n        </button>\n        <button ion-fab (click)="onClickMap(\'NORMAL\')" color="gpsc">\n          N\n        </button>\n      </ion-fab-list>\n    </ion-fab>\n\n    <!-- <ion-fab top right>\n      <button color="gpsc" mini ion-fab (click)="onSelectMapOption(\'mapHideTraffic\')">\n        <img src="assets/icon/trafficON.png" *ngIf="mapHideTraffic" />\n        <img src="assets/icon/trafficOFF.png" *ngIf="!mapHideTraffic" />\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 20%">\n      <button ion-fab mini (click)="onSelectMapOption(\'showGeofence\')" color="gpsc">\n        <img src="assets/imgs/geo.png" />\n      </button>\n    </ion-fab>\n    <ion-fab style="right: calc(10px + env(safe-area-inset-right)); margin-top: 33%">\n      <button ion-fab mini (click)="getPOIs()" color="gpsc">\n        <ion-icon name="pin"></ion-icon>\n      </button>\n    </ion-fab>\n    <ion-fab *ngIf="!isSuperAdmin || !isDealer" style="right: calc(10px + env(safe-area-inset-right)); margin-top: 33%">\n      <button ion-fab mini (click)="showCluster()" [ngStyle]="{\'background-color\': streetviewButtonColor}">\n        <ion-icon name="football" [ngStyle]="{\'color\': streetColor}"></ion-icon>\n      </button>\n    </ion-fab> -->\n  </div>\n</ion-content>\n\n<div *ngIf="onClickShow" class="divPlan">\n\n</div>\n'/*ion-inline-end:"/Users/apple/Desktop/white-labels/gpswale-ionic_1/src/pages/toll-map/toll-map.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"],
            __WEBPACK_IMPORTED_MODULE_2__providers_api_service_api_service__["a" /* ApiServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ElementRef"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ModalController"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ViewController"],
            __WEBPACK_IMPORTED_MODULE_4__ngx_translate_core__["c" /* TranslateService */]])
    ], TollMapPage);
    return TollMapPage;
}());

//# sourceMappingURL=toll-map.js.map

/***/ })

});
//# sourceMappingURL=18.js.map